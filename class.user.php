<?php
    class User {
        public $authorized = false;
        public $uid;
        public $username;

        public function __construct($dsn, $db_user, $db_pass) {
            $this->db = new PDO($dsn, $db_user, $db_pass);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

            if (isset($_SESSION['uid'])) {
                $this->authorized = true;
                $this->uid = $_SESSION['uid'];
                $this->username = $_SESSION['username'];
            } else if (isset($_POST['username']) && isset($_POST['password'])) {
                $user = $_POST['username'];
                $pass = $_POST['password'];
                $this->login($user, $pass);
            } else if (isset($_POST['newusername']) && isset($_POST['newpassword'])) {
                $user = $_POST['newusername'];
                $pass = $_POST['newpassword'];
                $this->register($user, $pass);
            }
        }


        private function login($user, $pass) {
            $st = $this->db->prepare('SELECT `uid`, `username`, `password`
                    FROM users  
                    LEFT JOIN users_approved  
                    ON users_approved.`uid` = users.`uid`  
                    WHERE (users_approved.`pending` IS NULL OR users_approved.`pending` <> 1)  
                    AND username = :u');
            $st->execute(array(':u' => $user));
            $row = $st->fetch();

            if ($row && $row->password == sha1($pass)) {
                $this->authorized = true;

                $this->uid = $row->uid;
                $_SESSION['uid'] = $this->uid;
                
                $this->username = $row->username;
                $_SESSION['username'] = $this->username;

                return true;
            } else {
                return false;
            }
        }


        private function register($user, $pass) {
            $pass = sha1($pass);

            $st = $this->db->prepare('INSERT INTO `users` (`username`, `password`) VALUES (:u, :p)');
            $st->execute(array(':u' => $user, ':p' => $pass));
            
            // Mark new account as unapproved
            $st = $this->db->prepare('SELECT `uid` FROM users WHERE `username` = :u');
            $st->execute(array(':u' => $user));
            $row = $st->fetch();

            $st = $this->db->prepare('INSERT INTO `users_approved` (`uid`, `pending`) VALUES (:u, 1)');
            $st->execute(array(':u' => $row->uid));            
        }

    }
?>
